# Jekyll toolbox

Run Jekyll and Bundle inside a Toolbox container.

## Prerequisites:

- `toolbox`
- `podman`

## Installation

### bash

```bash
bash <(curl -s https://gitlab.com/TheEvilSkeleton/jekyll-toolbox/-/raw/main/install.sh)
```

### fish

```fish
bash (curl -s https://gitlab.com/TheEvilSkeleton/jekyll-toolbox/-/raw/main/install.sh | psub)
```

## Setting up

### bash

```bash
alias bundle="toolbox run -c jekyll bundle"
alias jekyll="toolbox run -c jekyll jekyll"
alias gem="toolbox run -c jekyll gem"
```

### fish

```fish
alias bundle "toolbox run -c jekyll bundle"
alias jekyll "toolbox run -c jekyll jekyll"
alias gem "toolbox run -c jekyll gem"
```

## Uninstallation

```bash
podman kill jekyll
toolbox rm jekyll
```
