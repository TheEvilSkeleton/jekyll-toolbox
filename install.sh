#!/usr/bin/env bash

curl https://framagit.org/TheEvilSkeleton/jekyll-toolbox/-/raw/main/Dockerfile -o /tmp/Dockerfile

podman build -t jekyll-toolbox -f /tmp/Dockerfile /tmp
toolbox create -c jekyll --image jekyll-toolbox

# Ask if user wants to alias
printf "Do you want to create aliases?\n[y/n]: "
read -r alias

if [ "$alias" == "y" ]; then
	# Set XDG_CONFIG_HOME if empty
	if [ -z "$XDG_CONFIG_HOME" ]; then
		XDG_CONFIG_HOME="$HOME/.config"
	fi
	# Check if user is using bash. Skip otherwise
	if [ "$SHELL" == "/bin/bash" ] || [ "$SHELL" == "/usr/bin/bash" ]; then
		# Ask user to set bash conf file if needed
		printf "Enter the path of the bash configuration file you want to use (empty for ~/.bashrc): "
		read -r BASH_CONF
		# Set bash file to ~/.bashrc if empty variable
		if [ -z "$BASH_CONF" ]; then
			BASH_CONF="$HOME/.bashrc"
		fi
		# Create file if it doesn't exist
		if [ ! -f "$BASH_CONF" ]; then
			eval BASH_CONF="$BASH_CONF"
			touch "$BASH_CONF"
			echo Created "$BASH_CONF".
		fi
		# Set aliases
		{
		echo "alias bundle=\"toolbox run -c jekyll bundle\""
		echo "alias jekyll=\"toolbox run -c jekyll jekyll\""
		echo "alias gem=\"toolbox run -c jekyll gem\""
		} >> "$BASH_CONF"
		echo Successfully aliased in "$BASH_CONF".
	# Check if user is using zsh. Skip otherwise
	elif [ "$SHELL" == "/bin/zsh" ] || [ "$SHELL" == "/usr/bin/zsh" ]; then
		# Ask user to set bash conf file if needed
		printf "Enter the path of the zsh configuration file you want to use (empty for ~/.zshrc): "
		read -r ZSH_CONF
		# Set bash file to ~/.bashrc if empty variable
		if [ -z "$ZSH_CONF" ]; then
			ZSH_CONF="$HOME/.zshrc"
		fi
		# Create file if it doesn't exist
		if [ ! -f "$ZSH_CONF" ]; then
			eval ZSH_CONF="$ZSH_CONF"
			touch "$ZSH_CONF"
			echo Created "$ZSH_CONF".
		fi
		# Set aliases
		{
		echo "alias bundle=\"toolbox run -c jekyll bundle\""
		echo "alias jekyll=\"toolbox run -c jekyll jekyll\""
		echo "alias gem=\"toolbox run -c jekyll gem\""
		} >> "$ZSH_CONF"
		echo Successfully aliased in "$ZSH_CONF".
	# Check if user is using fish. Skip otherwise
	elif [ "$SHELL" == "/bin/fish" ] || [ "$SHELL" == "/usr/bin/fish" ]; then
		# Ask user to set fish conf file if needed
		printf "Enter the path of the fish configuration file you want to use (empty for ~/.config/fish/config.fish): "
		read -r FISH_CONF
		# Set fish file to ~/.config/fish/config.fish if empty variable
		if [ -z "$FISH_CONF" ]; then
			FISH_CONF="$XDG_CONFIG_HOME/fish/config.fish"
		fi
		# Create file if it doesn't exist
		if [ ! -f "$FISH_CONF" ]; then
			eval FISH_CONF="$FISH_CONF"
			touch "$FISH_CONF"
			echo Created "$FISH_CONF".
		fi
		# Set aliases
		{
		echo "alias bundle \"toolbox run -c jekyll bundle\""
		echo "alias jekyll \"toolbox run -c jekyll jekyll\""
		echo "alias gem \"toolbox run -c jekyll gem\""
		} >> "$FISH_CONF"
		echo Successfully aliased in "$FISH_CONF".
	else
		printf "Sorry! No supported shell found."
		printf "If you want to manually alias commands,"
		printf "add the following to your shell configuration file:"
		printf "alias bundle \"toolbox run -c jekyll bundle\""
		printf "alias jekyll \"toolbox run -c jekyll jekyll\""
		printf "alias gem \"toolbox run -c jekyll gem\""
	fi
fi
