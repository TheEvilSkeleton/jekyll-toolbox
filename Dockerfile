FROM docker.io/library/ruby

ENV NAME=jekyll-toolbox VERSION=1

LABEL com.github.containers.toolbox="true" \
      com.github.debarshiray.toolbox="true" \
      name="$NAME" \
      version="$VERSION" \
      usage="This image is meant to be used with the toolbox command" \
      summary="Base image for creating Jekyll toolbox containers"

RUN gem install \
    jekyll \
    bundler

RUN chmod -R 0777 /usr/local/bundle

CMD /bin/sh
